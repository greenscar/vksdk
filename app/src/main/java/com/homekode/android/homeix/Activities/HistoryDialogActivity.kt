package com.homekode.android.homeix.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.homekode.android.homeix.Fragments.HistoryDialogFragment
import com.homekode.android.homeix.HelpPack.RequestHelper
import com.homekode.android.homeix.HelpPack.RequestHelper.Companion.Shift
import com.homekode.android.homeix.HelpPack.RequestHelper.Companion.reqSendMessage
import com.homekode.android.homeix.HelpPack.RequestHelper.Companion.requestHistory
import com.homekode.android.homeix.Messages.Message
import com.homekode.android.homeix.Messages.MessageAdapter
import com.homekode.android.homeix.R

class HistoryDialogActivity : AppCompatActivity(),
        HistoryDialogFragment.HistoryDialogCallBack,
        RequestHelper.RHelperRequest{

    private lateinit var historyRecyclerView: RecyclerView

    private var offset = 0

    override fun dataTransfer(messages: ArrayList<Message>, fromRequestDialog: Boolean) {

        historyRecyclerView.layoutManager = LinearLayoutManager(applicationContext)
        historyRecyclerView.adapter = MessageAdapter(messages)
        historyRecyclerView.adapter.notifyDataSetChanged()
    }

    override fun loadHistory(id: Int, recyclerView: RecyclerView) {

        historyRecyclerView = recyclerView

        requestHistory(applicationContext, id, this@HistoryDialogActivity, offset)
    }

    override fun backLoad(id: Int) {
        if (offset > 0) {
            offset -= Shift
            requestHistory(applicationContext, id, this@HistoryDialogActivity, offset)
        }
    }

    override fun moreLoad(id: Int) {
        offset += Shift
        requestHistory(applicationContext, id, this@HistoryDialogActivity, offset)
    }

    override fun sendMessage(id: Int, message: String) {
       reqSendMessage(applicationContext, id, message)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_dialog)
    }


}
