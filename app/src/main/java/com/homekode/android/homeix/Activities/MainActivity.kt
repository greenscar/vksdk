package com.homekode.android.homeix.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.vk.sdk.VKScope
import com.vk.sdk.VKSdk
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKCallback
import android.content.Intent
import android.content.res.Configuration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.homekode.android.homeix.Fragments.DialogFragment
import com.homekode.android.homeix.Fragments.HistoryDialogFragment
import com.homekode.android.homeix.HelpPack.RequestHelper
import com.homekode.android.homeix.HelpPack.RequestHelper.Companion.Shift
import com.homekode.android.homeix.HelpPack.RequestHelper.Companion.reqSendMessage
import com.homekode.android.homeix.HelpPack.RequestHelper.Companion.requestDialogs
import com.homekode.android.homeix.HelpPack.RequestHelper.Companion.requestHistory
import com.homekode.android.homeix.Messages.Message
import com.homekode.android.homeix.Messages.MessageAdapter
import com.homekode.android.homeix.R
import com.vk.sdk.api.*


class MainActivity : AppCompatActivity(),
        DialogFragment.DialogFragmentCallBack,
        MessageAdapter.DialogAdapterCallBack,
        HistoryDialogFragment.HistoryDialogCallBack,
        RequestHelper.RHelperRequest{

    private lateinit var dialogRecyclerView: RecyclerView
    private lateinit var historyRecyclerView: RecyclerView

    private var offsetHist = 0
    private var offsetDialog = 0

    override fun dataTransfer(messages: ArrayList<Message>, fromRequestDialog: Boolean) {
        if (fromRequestDialog) {
            val adapter = MessageAdapter(messages)
            adapter.callBack = this@MainActivity

            dialogRecyclerView.layoutManager = LinearLayoutManager(applicationContext)
            dialogRecyclerView.adapter = adapter

            adapter.notifyDataSetChanged()

        } else {

            historyRecyclerView.layoutManager = LinearLayoutManager(applicationContext)
            historyRecyclerView.adapter = MessageAdapter(messages)
            historyRecyclerView.adapter.notifyDataSetChanged()

        }
    }

    override fun backLoad() {
        if(offsetDialog > 0) {
            offsetDialog -= Shift
            requestDialogs(applicationContext, this@MainActivity, offsetDialog)
        }
    }

    override fun moreLoad() {
        offsetDialog += Shift
        requestDialogs(applicationContext, this@MainActivity, offsetDialog)
    }

    override fun loginUp() {
        VKSdk.login(this, VKScope.MESSAGES)
    }

    override fun loadHistory(id: Int, recyclerView: RecyclerView) {
        historyRecyclerView = recyclerView
        requestHistory(applicationContext, id, this@MainActivity, offsetHist)
    }

    override fun backLoad(id: Int) {
        if (offsetHist > 0) {
            offsetHist -= Shift
            requestHistory(applicationContext, id, this@MainActivity, offsetHist)
        }
    }

    override fun moreLoad(id: Int) {
        offsetHist+= Shift
        requestHistory(applicationContext, id, this@MainActivity, offsetHist)

    }

    override fun sendMessage(id: Int, message: String) {
        reqSendMessage(applicationContext, id, message)
    }

    override fun loadHistory(id: Int) {

        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.frame_for_history_dialog, HistoryDialogFragment.createFragment(id))
                    .commit()
        } else {
            startActivity(Intent(applicationContext, HistoryDialogActivity::class.java).putExtra(HistoryDialogFragment.ID, id))
        }

    }

    override fun loadDialogs(recyclerView: RecyclerView) {

        dialogRecyclerView = recyclerView

        if (VKSdk.wakeUpSession(applicationContext)) {
            requestDialogs(applicationContext, this@MainActivity, 0)
        } else {
            VKSdk.login(this, VKScope.MESSAGES)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, object : VKCallback<VKAccessToken> {
            override fun onResult(res: VKAccessToken) {
                requestDialogs(applicationContext,this@MainActivity, 0)
            }
            override fun onError(error: VKError) {
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }


}
