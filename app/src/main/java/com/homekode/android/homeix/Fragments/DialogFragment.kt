package com.homekode.android.homeix.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homekode.android.homeix.R
import kotlinx.android.synthetic.main.fragment_dialog.*

/**
 * A simple [Fragment] subclass.
 */
class DialogFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?) =
            inflater!!.inflate(R.layout.fragment_dialog, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {

        if (activity !is DialogFragmentCallBack) {
            throw RuntimeException("This activity doesn't impliment DialogFragmentCallBack interface!")
        } else {

            val callBack = activity as DialogFragmentCallBack
            callBack.loadDialogs(recyc_dialog)
            back_btn.setOnClickListener {callBack.backLoad()}
            more_btn.setOnClickListener {callBack.moreLoad()}
            login_up_btn.setOnClickListener { callBack.loginUp() }
        }
    }

    interface DialogFragmentCallBack {
        fun loadDialogs(recyclerView: RecyclerView)
        fun backLoad()
        fun moreLoad()
        fun loginUp()
    }

}
