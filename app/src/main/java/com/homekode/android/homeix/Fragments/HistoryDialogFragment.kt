package com.homekode.android.homeix.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homekode.android.homeix.R
import kotlinx.android.synthetic.main.fragment_history_dialog.*

/**
 * A simple [Fragment] subclass.
 */
class HistoryDialogFragment : Fragment() {

   companion object {

       val ID = "id"

       fun createFragment(id: Int): Fragment {
           var bundle = Bundle()
           bundle.putInt(ID, id)
           val result = HistoryDialogFragment()
           result.arguments = bundle
           return result
       }

   }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?) =
            inflater!!.inflate(R.layout.fragment_history_dialog, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {

        if (activity !is HistoryDialogCallBack) {
            throw RuntimeException("This activity doesn't impliment HistoryDialogCallBack interface!")
        } else {

            val callBack = activity as HistoryDialogCallBack

            var id = if (arguments == null) activity.intent.getIntExtra(ID, 0)
            else arguments.getInt(ID, 0)
            callBack.loadHistory(id, recyc_history_dialog)

            send_message_btn.setOnClickListener { callBack.sendMessage(id, message_draft_txt.text.toString()) }
            back_btn.setOnClickListener { callBack.backLoad(id) }
            more_btn.setOnClickListener { callBack.moreLoad(id) }

        }

      /*  requestHistory(context,  if (arguments == null)activity.intent.getIntExtra(ID, 0)
        else arguments.getInt(ID, 0), recyc_history_dialog)*/
    }

    interface HistoryDialogCallBack {
        fun loadHistory(id: Int, recyclerView: RecyclerView)
        fun backLoad(id: Int)
        fun moreLoad(id: Int)
        fun sendMessage(id: Int, message: String)
    }

}
