package com.homekode.android.homeix.HelpPack

import android.app.Application
import com.vk.sdk.VKSdk

/**
 * Created by ASUS on 18.02.2018.
 */
class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()
        VKSdk.initialize(this)
    }
}