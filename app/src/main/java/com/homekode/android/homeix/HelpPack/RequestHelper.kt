package com.homekode.android.homeix.HelpPack

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.homekode.android.homeix.Messages.Message
import com.vk.sdk.api.*
import com.vk.sdk.api.model.VKApiDialog
import com.vk.sdk.api.model.VKApiGetDialogResponse
import com.vk.sdk.api.model.VKApiUser
import com.vk.sdk.api.model.VKList
import org.json.JSONObject

/**
 * Created by ASUS on 18.02.2018.
 */
class RequestHelper {

    interface RHelperRequest {
        fun dataTransfer(messages: ArrayList<Message>, fromRequestDialog: Boolean)
    }

    companion object {

        val Shift = 50

        private val DEFAULT = "User"

        fun requestDialogs(context: Context, implemCallBack: RHelperRequest, offset: Int) {

            val request = VKApi.messages().getDialogs(VKParameters.from(VKApiConst.OFFSET, offset,
                    VKApiConst.COUNT, Shift,
                    VKApiConst.PREVIEW_LENGTH, 30))

            request.executeWithListener(object : VKRequest.VKRequestListener() {
                override fun onComplete(response: VKResponse?) {
                    requestUserInfo( VKlisToDoalogList((response!!.parsedModel as VKApiGetDialogResponse).items),
                            implemCallBack, true)
                }

                override fun onError(error: VKError?) {
                    Toast.makeText(context, "Error " + error.toString(), Toast.LENGTH_SHORT).show()
                }

                override fun attemptFailed(request: VKRequest?, attemptNumber: Int, totalAttempts: Int) {
                    Toast.makeText(context, "Attempt Fail" , Toast.LENGTH_SHORT).show()
                }
            })
        }

        private fun VKlisToDoalogList(VKList: VKList<VKApiDialog>): ArrayList<Message> {

            val dialogs = arrayListOf<Message>()
            VKList.mapTo(dialogs) { Message(it.message.user_id, it.message.body, DEFAULT) }
            return dialogs
        }

        fun requestHistory(context: Context, id: Int, implemCallBack: RHelperRequest, offset: Int) {

            val request = VKRequest("messages.getHistory",
                    VKParameters.from(VKApiConst.OFFSET, offset,
                            VKApiConst.COUNT, Shift,
                            VKApiConst.USER_ID, "" + id))
            request.executeWithListener(object : VKRequest.VKRequestListener() {
                override fun onComplete(response: VKResponse?) {
                    requestUserInfo( dialogsFromJson(JSONObject(response!!.responseString)
                            .getJSONObject("response")), implemCallBack, false)
                }

                override fun onError(error: VKError?) {
                    Toast.makeText(context, "Error " + error.toString(), Toast.LENGTH_SHORT).show()
                }

                override fun attemptFailed(request: VKRequest?, attemptNumber: Int, totalAttempts: Int) {
                    Toast.makeText(context, "Attempt Fail" , Toast.LENGTH_SHORT).show()
                }
            })
        }

        private fun dialogsFromJson(rootJson: JSONObject): ArrayList<Message> {

            val dialogs = arrayListOf<Message>()
            val items = rootJson.getJSONArray("items")

            (0 until items.length()).mapTo(dialogs) {
                Message(items.getJSONObject(it).getInt("from_id"),
                        items.getJSONObject(it).getString("body"), DEFAULT)
            }

            return dialogs
        }

        fun reqSendMessage(context: Context, id: Int, messageBody: String) {

            val request = VKRequest("messages.send",
                    VKParameters.from(VKApiConst.USER_ID, "" + id, VKApiConst.MESSAGE, messageBody))
            request.executeWithListener(object : VKRequest.VKRequestListener() {
                override fun onComplete(response: VKResponse?) {}

                override fun onError(error: VKError?) {
                    Toast.makeText(context, "Error " + error.toString(), Toast.LENGTH_SHORT).show()
                }

                override fun attemptFailed(request: VKRequest?, attemptNumber: Int, totalAttempts: Int) {
                    Toast.makeText(context, "Attempt Fail" , Toast.LENGTH_SHORT).show()
                }
            })

        }

        private fun requestUserInfo(messages: ArrayList<Message>, implemCallBack: RHelperRequest, fromRequestDialog: Boolean)  {

            var user_id = ""

            for (i in 0 until messages.size) {
                user_id = if (i != messages.size) { user_id  + messages[i].user_id + ", " }
                        else { user_id  + messages[i].user_id }
            }

            val request = VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS, user_id))
            request.executeWithListener(object : VKRequest.VKRequestListener() {

                override fun onComplete(response: VKResponse?) {

                    val data = (response!!.parsedModel as VKList<VKApiUser>)

                    for (dat in data) { messages.filter { dat.id == it.user_id }.forEach { it.name = dat.first_name } }

                    implemCallBack.dataTransfer( messages, fromRequestDialog)
                }
                override fun onError(error: VKError?) {}
                override fun attemptFailed(request: VKRequest?, attemptNumber: Int, totalAttempts: Int) {}
            })
        }


    }
}