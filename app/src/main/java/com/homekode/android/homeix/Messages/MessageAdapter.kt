package com.homekode.android.homeix.Messages

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homekode.android.homeix.R
import kotlinx.android.synthetic.main.item_dialog.view.*

/**
 * Created by ASUS on 18.02.2018.
 */

class MessageAdapter(var data: ArrayList<Message>) : RecyclerView.Adapter<MessageAdapter.UserViewHolder>() {

    var callBack: DialogAdapterCallBack? = null

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            UserViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_dialog, parent, false))

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {


        holder.usetID.text = data[position].name
        holder.body.text = data[position].body

        holder.itemView.setOnClickListener {

            if (callBack != null) {
                callBack!!.loadHistory(data[position].user_id)
            }
        }
    }

    class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var usetID = view.user_id
        var body = view.body
    }

    interface DialogAdapterCallBack {
        fun loadHistory(id: Int)
    }

}